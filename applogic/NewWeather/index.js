const express = require('express')
const app = express()
const openWeatherAPI = require('./openWeatherAPI')
const weather = new openWeatherAPI()
const catalyst = require('zcatalyst-sdk-node')

app.use(express.json())



app.get('/weather/:city', (req, res) => {
    const city = req.params.city
    weather.getWeather(city).then((response) => {
        console.log(response)
        res.writeHead(200, { "Content-Type": "application/json" })
        res.write(JSON.stringify(response))
        res.end()
    }).catch((error) => {
        console.log(error)
        res.writeHead(500).end(error)
    })
})

app.get('/email', (req, res) => {
    const catalystApp = catalyst.getInstance(req, { type: catalyst.type.request })
    console.log('DB')
    catalystApp.datastore().table('Subscribers').getAllRows().then((rows) => {
        console.log("DBCalled")
        rows.forEach(row => {
            console.log(row)
            console.log(row.city)
            weather.getWeather(row.city).then((response) => {
                const mail = {
                    from_email: "giridhar.vc@zohocorp.com",
                    to_email: row.email,
                    subject: "Current Weather",
                    content: "Its " + response.main.temp + "Outside"
                }
                console.log('Sending mail')
                catalystApp.email().sendMail(mail).then(() => {
                    res.writeHead(200, { "Content-Type": "application/json" })
                    res.write(JSON.stringify(response))
                    res.end()
                }).catch((error) => {
                    console.log(error)
                })
            }).catch((error) => {
                console.log(error)
                res.writeHead(500).end(error)
            })

        });
    }).catch((error) => {
        res.writeHead(200, { "Content-Type": "text" })
        res.write(error)
        res.end()
    })
})

app.get('/subscribe', (req, res) => {
    const email = req.query.email
    const city = req.query.city
    console.log(email, city)
    const catalystApp = catalyst.getInstance(req, { type: catalyst.type.request });

    catalystApp.datastore().table('Subscribers').insertRow({
        email: email,
        city: city
    })

    res.writeHead(200, { "Content-Type": "application/json" })
    const resp = {status: "Email Subscribed"}
    res.write(JSON.stringify(resp))
    res.end()

})

module.exports = app



