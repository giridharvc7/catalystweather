const request = require('request')

class openWeatherAPI {
    getWeather(city){
        return new Promise((resolve, reject) =>{
            console.log(city)
            request.get({
                url: "https://api.openweathermap.org/data/2.5/weather?q=" + city + "&appid=d38c9cf1811c648b86d94f30225f546f&units=metric"
            },(error, response,body)=>{
                const resp = JSON.parse(body)
                if (error)
                {
                    return reject(error)
                }
                if (response.statusCode == 200)
                {
                    const weather = {main: resp.main, 
                                     condition: this.normalizeCondition(resp.weather[0].main),
                                     description: resp.weather[0].description}
                    return resolve(weather)
                }
                else{
                    return reject("Error in OpenWeather")
                }
            })
        })
    }

    normalizeCondition(condition){
        const cond = condition.toLowerCase()
        if (cond.includes('rain') || cond.includes('thunderstorm') || cond.includes('drizzle'))
        {
            return "rain"
        }
        if (cond.includes('clouds') || cond.includes('haze') || cond.includes('fog'))
        {
            return "cloudy"
        }
        if(cond.includes('snow'))
        {
            return "snow"
        }
        if (cond.includes('clear'))
        {
            return "sunny"
        }
        
        return "default"
    }
}

module.exports = openWeatherAPI