const submit = (e) =>{
    if (e.keyCode == 13)
    {
        search_weather()
        e.preventDefault()
    }
}

const search_weather = ()=>{
    const city = $('.city').val();
    // fetch('http://localhost:3000/server/weather/weather/'+encodeURIComponent(city)).then((response) =>{

    // })

    fetch("/server/weather/weather/"+encodeURIComponent(city))
    .then(response => response.json())
    .then(resp => {
        $(".txtarea").show();
        $(".subsbtn").show();
        const temperature = resp.main.temp
        const newCondition = resp.condition

        $("#celsius").text(temperature)
        $("#country").text(city)
        $("#faren").text("/ " + farenheit(temperature))

        changeWeather(newCondition)
        return false 
    })
    return false 
}

const subscribeCity = ()=>{
    const city = $('.city').val();
    const email = $('#email').val();

    subscribe(city, email)
}

const subscribe = (city, email)=>{
    fetch("/server/weather/subscribe?email="+encodeURIComponent(email)+"&city="+encodeURIComponent(city))
    .then(response => console.log(response)).catch( (err) =>{
        console.log(err)
    })
}


var farenheit = (celsius) =>{
    return Number(((celsius)*(9/5)) + 32).toFixed(2)
}


var changeWeather = (newC)=>{
		$(document).ready(()=>{
            $(".season").addClass("common")
			$('.'+newC).removeClass('common');
	})}	

